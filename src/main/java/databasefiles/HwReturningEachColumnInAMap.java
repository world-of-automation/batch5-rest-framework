package databasefiles;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static databasefiles.ConnectToSqlDB.connection;

public class HwReturningEachColumnInAMap {

    public static Logger logger = LogManager.getLogManager().getLogger(String.valueOf(HwReturningEachColumnInAMap.class));

    public static Map<String, String> getMapFromDBOfSingleRecord(String query) {
        ConnectToSqlDB.establishDBConnection();
        logger.info("Connection Successful");

        Map<String, String> data = new HashMap<>();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            ResultSetMetaData rsMetaData = resultSet.getMetaData();

            while (resultSet.next()) {
                for (int i = 1; i <= rsMetaData.getColumnCount(); i++) {
                    String value = null;
                    if (value == null) {
                        value = resultSet.getString(rsMetaData.getColumnName(i));
                        data.put(rsMetaData.getColumnName(i), value);
                    }
                }
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }
        return data;
    }
}


