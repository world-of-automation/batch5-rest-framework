package databasefiles;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.List;

public class ConnectToSqlDB {
    public static Connection connection = null;
    public static PreparedStatement ps = null;
    private static List<String> list;

    public static void establishDBConnection() {
        try {
            String username = FileReaderUtils.getPropertyOfFile("src/main/resources/secret.properties", "username");
            String password = FileReaderUtils.getPropertyOfFile("src/main/resources/secret.properties", "password");
            String url = "jdbc:mysql://localhost:3306/worldOfAutomation";
            connection = DriverManager.getConnection(url, username, password);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}