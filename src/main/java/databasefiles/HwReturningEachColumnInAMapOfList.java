package databasefiles;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static databasefiles.ConnectToSqlDB.connection;

public class HwReturningEachColumnInAMapOfList {

    public static Logger logger = Logger.getLogger(HwReturningEachColumnInAMapOfList.class.toString());

    public static ArrayList<Map> getMapFromDBOfSingleRecord(String query) {
        ConnectToSqlDB.establishDBConnection();
        logger.info("Connection Successful");

        ArrayList<Map> listOfData = new ArrayList<>();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            ResultSetMetaData rsMetaData = resultSet.getMetaData();

            while (resultSet.next()) {
                Map<String, String> singleData = new HashMap<>();
                for (int i = 1; i <= rsMetaData.getColumnCount(); i++) {
                    String value = null;
                    if (value == null) {
                        value = resultSet.getString(rsMetaData.getColumnName(i));
                        singleData.put(rsMetaData.getColumnName(i), value);
                    }
                }
                listOfData.add(singleData);
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }
        return listOfData;
    }
}


