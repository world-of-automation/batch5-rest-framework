package databasefiles;

import lombok.Data;

import java.util.List;

@Data
public class Test3 {

    private String timestamp;
    private String feature;
    private int id;
    private List<databasefiles.Data> data;

}
