package databasefiles;

import lombok.Data;

@Data
public class User {

    private String name;
    private String teamName;
    private String salary;

}
