package databasefiles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.annotations.Test;

import java.io.FileReader;

public class JsonFileProcessing {

    @SneakyThrows
    public static JSONArray getJsonArray(String path) {
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(path));
        return jsonArray;
    }

    @SneakyThrows
    @Test
    public void readSingleArray_SingleJson() {
        JSONArray jsonArray = getJsonArray("src/main/resources/SingleArray_SingleJson.json");
        System.out.println(jsonArray.toJSONString());

        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
        System.out.println(jsonObject);
        System.out.println(jsonObject.get("data"));
    }

    @SneakyThrows
    @Test
    public void readSingleArray_MultipleJson() {
        JSONArray jsonArray = getJsonArray("src/main/resources/SingleArray_MultipleJson.json");
        System.out.println(jsonArray.toJSONString());

        JSONObject jsonObject = (JSONObject) jsonArray.get(1);
        System.out.println(jsonObject);
        System.out.println(jsonObject.get("data"));
    }

    @SneakyThrows
    @Test
    public void readSingleArray_JsonInMultipleJson() {
        JSONArray jsonArray = getJsonArray("src/main/resources/SingleArray_JsonInMultipleJson.json");
        System.out.println(jsonArray.toJSONString());

        JSONObject jsonObject = (JSONObject) jsonArray.get(1);
        System.out.println(jsonObject);
        JSONObject rohanDetail = (JSONObject) jsonObject.get("data");
        System.out.println(rohanDetail.get("name"));
    }


    @SneakyThrows
    @Test
    public void readSingleArray_ArrayInMultipleJson() {
        JSONArray jsonArray = getJsonArray("src/main/resources/SingleArray_ArrayInMultipleJson.json");
        System.out.println(jsonArray.toJSONString());

        JSONObject jsonObject = (JSONObject) jsonArray.get(1);
        System.out.println(jsonObject);
        JSONArray data = (JSONArray) jsonObject.get("data");

        System.out.println(data);
        JSONObject naimDetail = (JSONObject) data.get(1);
        String name = naimDetail.get("name").toString();

    }


    @Test
    public void printMyName() {
        String name = "";
        System.out.println(name);
    }


    @SneakyThrows
    @Test
    public void readTestJson() {

        String env = System.getProperty("environment");
        System.out.println(env);


        JSONArray jsonArray = getJsonArray("src/main/resources/Test.json");
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            String environment = jsonObject.get("env").toString();

            if (environment.equalsIgnoreCase(env)) {
                JSONObject data = (JSONObject) jsonObject.get("data");
                System.out.println(data.get("name"));
            }
        }
    }


    @Test
    public void useClassToStoreJson() throws JsonProcessingException {
        JSONArray jsonArray = getJsonArray("src/main/resources/User.json");
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);

        ObjectMapper objectMapper = new ObjectMapper();
        User user = objectMapper.readValue(jsonObject.toJSONString(), User.class);

        System.out.println(user.toString());
        System.out.println(user.getName());
        System.out.println(user.getSalary());
        System.out.println(user.getTeamName());


    }

    @Test
    public void useTest3ToStoreJson() throws JsonProcessingException {
        JSONArray jsonArray = getJsonArray("src/main/resources/Test3.json");
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);

        ObjectMapper objectMapper = new ObjectMapper();
        Test3 test = objectMapper.readValue(jsonObject.toJSONString(), Test3.class);

        System.out.println(test.toString());
        System.out.println(test.getData().get(0).getName());
        System.out.println(test.getData().get(1).getName());


    }

}
