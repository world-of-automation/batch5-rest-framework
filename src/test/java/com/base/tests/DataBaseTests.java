package com.base.tests;


import com.base.ExtentTestManager;
import com.base.TestBase;
import databasefiles.HwReturningEachColumnInAMap;
import databasefiles.HwReturningEachColumnInAMapOfList;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;

public class DataBaseTests extends TestBase {
    private static final Logger logger = Logger.getLogger(String.valueOf(HwReturningEachColumnInAMap.class));

    @Test
    public void validateTestRun() {
        ArrayList<Map> data = HwReturningEachColumnInAMapOfList.getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee");
        System.out.println(data);

        // Map<String, String> data2 = HwReturningEachColumnInAMap.getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee");
        // System.out.println(data2);
    }


    @Test
    public void validateUserCanGetAllEmployees() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/all";
        Response response = RestAssured
                .given()
                .when()
                .get(endpoint)
                .then()
                .log()
                .all()
                .extract().response();
        ExtentTestManager.log(response.asString());
    }


    //@Test
    public void validateUserCanGetSpecificEmployees() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/1";
        Response response = RestAssured
                .given()
                .when()
                .get(endpoint)
                .then()
                .log()
                .all()
                .extract().response();
        ExtentTestManager.log(response.asString());
    }


    //@Test
    public void validateUserCanDeleteAnEmployee() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/delete/4";
        Response response = RestAssured
                .given()
                .when()
                .delete(endpoint)
                .then()
                .log()
                .all()
                .extract().response();
        ExtentTestManager.log(response.asString());
    }


    // @Test
    public void validateUserCanCreateAnEmployee() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/add";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Sub");
        jsonObject.put("permanent", 1);
        jsonObject.put("phone_number", 345363);
        jsonObject.put("role", "QA");
        jsonObject.put("city", "Ozone Park");

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);

        System.out.println(jsonArray.toJSONString());

        Response response = RestAssured
                .given()
                .header("Content-Type", "application/json")
                .when()
                .body(jsonArray.toJSONString())
                .post(endpoint)
                .then()
                .log()
                .all()
                .extract().response();

        ExtentTestManager.log(response.asString());

        //   ObjectMapper objectMapper = new ObjectMapper();
        //  Test test = objectMapper.readValue(response.asString(),Test.class);
        //
        //  Testdb testdb = objectMapper.readValue(mapOfDataFromDB,Testdb.class);
        //
        // Assert.assertEqueals(test.getName(),testDB.getName());

    }


    //@Test
    public void validateUserCanUpdateAnEmployee() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/update/9";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Sub");
        jsonObject.put("permanent", 1);
        jsonObject.put("phone_number", 345363);
        jsonObject.put("role", "QA");
        jsonObject.put("city", "Brooklyn");

        Response response = RestAssured
                .given()
                .header("Content-Type", "application/json")
                .when()
                .body(jsonObject.toJSONString())
                .put(endpoint)
                .then()
                .log()
                .all()
                .extract().response();

        ExtentTestManager.log(response.asString());

        // query db
        // from the map get all records --> expected
        // parse the response json into a map
        // and match  them all with the records


        // try to create a employee with different phone numbers
        // 9,10,11, empty, null, alphanumeric, alpha, special char, alpha & special char, with space

        // storing response json into object/pojo using jackson-databind

    }


}

